package com.jose.tegra.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jose.tegra.domain.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
	@Query("SELECT obj FROM Cliente obj WHERE obj.cpfOuCnpj = :cpf_Cnpj")
	public Cliente consultarCliente_cpf_cnpj_repo(@Param("cpf_Cnpj") String cpf_Cnpj);
}
