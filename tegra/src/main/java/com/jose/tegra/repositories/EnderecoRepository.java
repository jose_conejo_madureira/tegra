package com.jose.tegra.repositories;

import java.util.List;

import javax.validation.constraints.Null;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jose.tegra.domain.Cidade;
import com.jose.tegra.domain.Cliente;
import com.jose.tegra.domain.Endereco;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

	@Query("SELECT obj FROM Endereco obj WHERE obj.cep = :cep")
	public List<Endereco> ConsultarCep_repo(@Param("cep") String cep);
	
}
