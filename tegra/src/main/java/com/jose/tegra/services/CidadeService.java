package com.jose.tegra.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jose.tegra.domain.Cidade;
import com.jose.tegra.repositories.CidadeRepository;

@Service
public class CidadeService {
	@Autowired
	private CidadeRepository repo;
	
	public List<Cidade> ConsultarMunicipioService(Integer estado_id)   {
		
		
		return repo.findCidades(estado_id);
	
	}	
	
}
