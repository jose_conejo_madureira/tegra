package com.jose.tegra.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.jose.tegra.domain.Estado;
import com.jose.tegra.repositories.EstadoRepository;

@Service
public class EstadoService {
	@Autowired
	private EstadoRepository repo;
	
	public List<Estado> ConsultarEstadoService() throws JsonMappingException, JsonProcessingException {
		List<Estado> obj = repo.findAll();
		return obj;
		
	}
	
	
}
