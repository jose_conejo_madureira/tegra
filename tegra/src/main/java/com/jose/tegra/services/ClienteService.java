package com.jose.tegra.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jose.tegra.domain.Cliente;
import com.jose.tegra.repositories.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepository  repoCliente;
	
	public Cliente buscar_cpf_cnpj_service(String cpf_cnpj) {
		
		Cliente obj = repoCliente.consultarCliente_cpf_cnpj_repo(cpf_cnpj);
		return obj;

	}
	
	
}
