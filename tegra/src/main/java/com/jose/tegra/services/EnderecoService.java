package com.jose.tegra.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jose.tegra.domain.Cidade;
import com.jose.tegra.domain.Cliente;
import com.jose.tegra.domain.Endereco;
import com.jose.tegra.repositories.CidadeRepository;
import com.jose.tegra.repositories.EnderecoRepository;

@Service
public class EnderecoService {
	
	
	@Autowired
	private EnderecoRepository repo;
	
	public List<Endereco> consultarCep_service(String cep) {
		
		List<Endereco> obj = repo.ConsultarCep_repo(cep);
		return obj;

	}
	public void alteraEnderecoService(String cep) {
		

	}
}
