package com.jose.tegra;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;	
import com.jose.tegra.domain.*;
import com.jose.tegra.repositories.*;


@SpringBootApplication
public class TegraApplication implements CommandLineRunner {
	
	@Autowired
	private  CidadeRepository cidadeRepository;

	@Autowired
	private  ClienteRepository clienteRepository;

	@Autowired
	private  EnderecoRepository enderecoRepository;
	
	@Autowired
	private  EstadoRepository estadoRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(TegraApplication.class, args);
	}
	
	
	public void run(String... args) throws Exception {
		
		Estado est1 = new Estado(null,"Sao Paulo");
		Estado est2 = new Estado(null,"Minas Gerais");

		Cidade c1 = new Cidade("são bernadro do campo", est1);
		Cidade c2 = new Cidade("belo horizonte", est2);

		est1.getCidades().addAll(Arrays.asList(c1));
		est2.getCidades().addAll(Arrays.asList(c2));
	
		

		Cliente cli1 = new Cliente("jose", "jose@gmail", "41591354803");
		Cliente thais = new Cliente("thais", "thais@gmail", "123456");

		
		cli1.getTelefones().addAll(Arrays.asList("27363323", "93838393"));
		Endereco e1 = new Endereco( "Rua Flores", "300", "Apto 303", "Jardim", "38220834", cli1, c1);
		Endereco e2 = new Endereco( "Avenida Matos", "105", "Sala 800", "Centro", "38777012", cli1, c2);

		cli1.getEnderecos().addAll(Arrays.asList(e1, e2));


		thais.getTelefones().addAll(Arrays.asList("27363323", "93838393"));
		Endereco e3 = new Endereco( "Rua castro alves", "6", "Apto 303", "casa", "38220834", thais, c1);
		Endereco e4 = new Endereco( "rua castro alves", "4", "Sala 800", "Centro", "38777012", thais, c2);
		
		thais.getEnderecos().addAll(Arrays.asList(e3, e4));
	
		clienteRepository.save(thais) ;
		estadoRepository.saveAll(Arrays.asList(est1, est2));
		cidadeRepository.saveAll(Arrays.asList(c1, c2));
		clienteRepository.saveAll(Arrays.asList(cli1,thais));
		enderecoRepository.saveAll(Arrays.asList(e1, e2,e3,e4));
		
	
	}

}
