package com.jose.tegra.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.jose.tegra.domain.Cidade;
import com.jose.tegra.services.CidadeService;

@RestController
@RequestMapping(value = "/cidade")
public class CidadeResource {
	
	@Autowired
	private CidadeService service;

	@RequestMapping(value = "/{estado_id}", method = RequestMethod.GET)
	public List<Cidade> cidadeReso(@PathVariable Integer estado_id) throws JsonMappingException, JsonProcessingException {
		
		
		List<Cidade> list = service.ConsultarMunicipioService(estado_id);
		return list;
	}

}
