package com.jose.tegra.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jose.tegra.domain.Cidade;
import com.jose.tegra.domain.Endereco;
import com.jose.tegra.services.EnderecoService;

@RestController
@RequestMapping(value = "/endereco")
public class EnderecoResource {
	@Autowired
	
	private EnderecoService service;
	
	@RequestMapping(value = "/{cep}", method = RequestMethod.GET)
	public List<Endereco> ConsultarCep(@PathVariable String cep) {
		
		List<Endereco> obj = service.consultarCep_service(cep);
		
		return obj;
	}
	
	@RequestMapping(value = "/altera/{cep}", method = RequestMethod.GET)
	public void altera(@PathVariable String cep) {
		service.alteraEnderecoService(cep);
	}
}
