package com.jose.tegra.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jose.tegra.domain.Cliente;
import com.jose.tegra.services.ClienteService;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteResource {
	@Autowired
	
	private ClienteService service;

	@RequestMapping(value = "/{cpf_cnpj}", method = RequestMethod.GET)
	public Cliente find(@PathVariable String cpf_cnpj) {
		
		Cliente obj = service.buscar_cpf_cnpj_service(cpf_cnpj);
		
		return obj;
	}
	
	
}
